﻿using System.Text.Json.Serialization;

namespace MocoMakers;

public record MakerList(
    [property: JsonPropertyName("makers")] List<Maker> Makers);