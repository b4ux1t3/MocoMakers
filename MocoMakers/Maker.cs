﻿using System.Text.Json.Serialization;

namespace MocoMakers;

public record Maker(
    [property: JsonPropertyName("name")]string Name, 
    [property: JsonPropertyName("twitch")]string TwitchName, 
    [property: JsonPropertyName("discord")]string DiscordName,
    [property: JsonPropertyName("website")]string WebSite,
    [property: JsonPropertyName("twitch_channel")]string? TwitchChannel,
    [property: JsonPropertyName("etsy")]string? Etsy,
    [property: JsonPropertyName("description")]string Description);