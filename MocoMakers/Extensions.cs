﻿using System.Text;

namespace MocoMakers;

public static class Extensions
{
    public static string ToHtml(this Maker maker)
    {
        var sb = new StringBuilder();

        sb.Append("<div>");

        sb.Append($"<table><tbody>");
        // sb.Append($"<tr><th scope=\"col\" class=\"table-head cat-head\">{maker.Name}</th><th class=\"table-head\"></th></tr>");
        sb.Append($"<tr><th scope=\"col\" class=\"table-head cat-head\" colspan=2>{maker.Name}</th></tr>");
        sb.Append($"<tr><th class=\"cat-head\" scope=\"row\">Description</th><td>{maker.Description.Replace("@", string.Empty)}</td></tr>");
        sb.Append($"<tr><th class=\"cat-head\" scope=\"row\">Discord Name</th><td>{maker.DiscordName}</td></tr>");
        sb.Append($"<tr><th class=\"cat-head\" scope=\"row\">Twitch Name</th><td>{maker.TwitchName}</td></tr>");
        sb.Append($"<tr><th class=\"cat-head\" scope=\"row\">Website</th><td><a href=\"{maker.WebSite}\">Link</a></td></tr>");
        if (maker.Etsy is not null)
            sb.Append($"<tr><th class=\"cat-head\" scope=\"row\">Etsy</th><td><a href=\"{maker.Etsy}\">Link</a></td></tr>");
        if (maker.TwitchChannel is not null)
            sb.Append($"<tr><th class=\"cat-head\" scope=\"row\">Twitch Channel</th><td><a href=\"{maker.TwitchChannel}\">Link</a></td></tr>");
        
        
        sb.Append("</tbody></table>");
        sb.Append("</div>\n");

        return sb.ToString();
    }
}