﻿// See https://aka.ms/new-console-template for more information

using System.Text.Json;
using MocoMakers;

if (args.Length < 1)
{
    Console.Error.WriteLine("Need an input file.");
    return 1;
}

var stream = File.Open(args[0], FileMode.Open);
var makers = JsonSerializer.Deserialize<MakerList>(stream);
var makerDivs = makers.Makers.Select(m => m.ToHtml()).Aggregate((d, d1) => $"{d}{d1}");
var htmlOut = File.ReadAllText("template.html");
htmlOut = htmlOut.Replace("{{MOCOGOHERE}}", makerDivs);
var outFile = args.Length > 1 ? $"{args[1]}" : "makers.html";
File.WriteAllText(outFile, htmlOut);


return 0;